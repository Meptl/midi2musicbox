# midi2musicbox

Converts midi into a music box cylinder for a music box.
The resulting 3d model is intended to be 3d-printed.

![](./musicbox.webp)

The current setup is for your typical 18-tooth comb which comes with a removable
gear section of the cylinder.
I used and disassembled [this](https://www.amazon.com/dp/B0CDC7NYQ2).
However I imagine [this](https://www.aliexpress.us/item/3256802637004046.html)
is the exact same item.
I cut the rivets with micro-flush cutters.

# Setup
If not using nix flakes.
`pip install mido`
and ensure you have `openscad` available.

Update `main.py` note_mapping as needed.

# Usage
`python main.py Automne-short.mid`

This will produce `main.stl` using OpenSCAD.

I've also added `rivet-replacement.stl` to hold the handcrank in place.


# Considerations
18-note music boxes are typically tuned to specific songs.
As such `main.py` has a dictionary mapping midi note values to tooth index.
I'm in search of an untuned large music box comb.

There exists a minimum distance that must be maintained between two of the same notes.

There is a rattle that occurs when the cylinder teeth contacts a
currently ringing music box tooth.

The default `lip_thickness` is a little small so the two notes closest to the gear head may have issues printing.
The size was necessary to mate properly with the gear head.
