import os
import sys

from mido import MidiFile

if len(sys.argv) != 2:
    print("USAGE: main.py MIDI_FILE")
    exit(1)

# The output is 1-indexed.
note_mapping = {
    # 60: 1, # Fuuuu
    # 62: 2, # Fuuuu
    64: 3,
    65: 4,
    # 66: 5, # Fuuuu
    67: 5,
    69: 6,
    # 70: 7, # Fuuuu
    71: 7,
    72: 8,
}

def process_midi_file(file_path):
    midi_file = MidiFile(file_path)

    song_duration = 0
    for track in midi_file.tracks:
        for msg in track:
            song_duration += msg.time

    pos = []
    notes = []
    cumulative_delta_time = 0
    for track in midi_file.tracks:
        for msg in track:
            if msg.type == 'note_on' and msg.velocity > 0:
                normalized_time = cumulative_delta_time / song_duration
                if not msg.note in note_mapping:
                    print(f"Aw man we don't have a mapping for note {msg.note}")
                    exit(1)
                    break

                pos.append(normalized_time)
                notes.append(note_mapping[msg.note])
            cumulative_delta_time += msg.time

    pos_formatted = "[{}]".format(','.join([f"{num:.6f}" for num in pos]))
    notes_formatted = "[{}]".format(','.join([f"{num}" for num in notes]))

    cmd = f'openscad -o main.stl -D positions="{pos_formatted}" -D notes="{notes_formatted}" main.scad'
    print(cmd)
    os.system(cmd)

process_midi_file(sys.argv[1])
