fudge = 0.01;
tube_diam = 13.3;

// positions and notes match 1:1 based on index. With position dictating the
// number of notes.
pos_beat = [0,1,6,7,9,10,12,13,15,16,18,19];
positions = [for (b = pos_beat) b * 0.03];
// positions = [];

// Not zero indexed.
notes = [3,6,8,7,8,6,5,6,5,4,5,3];



// Assumes uniform teeth size.
comb_size = 16.2;
comb_teeth = 18;
comb_teeth_spacing = 0.03;
comb_teeth_size = (comb_size - comb_teeth_spacing*(comb_teeth-1))/comb_teeth;

module tube() {
    height=21.07;
    bottom_thickness = 1.2;
    bottom_hole = 5.27;
    bottom_hole_top = 4.77;
    lip_thickness = 0.65;
    lip_size = 4.5;
    wall_thickness = 1.5;
    $fn=64;

    difference() {

        cylinder(h=height, d=tube_diam);

        translate([0,0, height-lip_size]) cylinder(h=height, d=tube_diam-lip_thickness);
        translate([0,0, bottom_thickness]) cylinder(h=height, d=tube_diam-wall_thickness);
        translate([0,0,-fudge]) cylinder(h=bottom_thickness+fudge*2,d1=bottom_hole,d2=bottom_hole_top);
    };
}

module finger(finger_size) {
    difference() {
        cube([finger_size, finger_size*0.8, finger_size], center=true);
        translate([0,0,-finger_size*1.4]) rotate([0,-22,0]) cube(finger_size * 2, center=true);
        translate([0,0,finger_size*1.4]) rotate([0,22,0]) cube(finger_size * 2, center=true);
    }
}

module fingers(positions, notes) {
    finger_size = 0.7;
    note_offset = 2.85;

    // We want to embed the finger slightly into the wall. It is a function of
    // finger size and tube diameter so I've just eyeballed it.
    x_offset = tube_diam/2 + finger_size/2 - finger_size*0.1;
    for (i = [0:len(positions)-1]) {
        pos = positions[i];
        note = notes[i];

        // Remember, note is 1-indexed.
        z_offset = comb_size - comb_teeth_size * note - comb_teeth_size/2 - comb_teeth_spacing * (note - 1) + finger_size/2 + note_offset;
        // We smudge the rotate enough to make the cube edge start at 0.
        // This was eyeballed and should be a function of finger size.
        // Note that we don't simply remove center=true because that makes the
        // cube angle invalid.
        rotate([0,0,-360*pos-3])
            translate([x_offset,0,z_offset])
                finger(finger_size);
    }

    // One finger outside of the play area for debugging.
    translate([x_offset,0,finger_size/2]) finger(finger_size);
}

union() {
    tube();
    fingers(positions, notes);
}
